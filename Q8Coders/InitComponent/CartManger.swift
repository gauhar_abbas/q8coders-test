//
//
//  Created by Gauhar Abbas on 01/12/18.
//  Copyright © 2018 Q8Coders. All rights reserved.
//

import Foundation

public class CartManager {
    
    public static var inst: CartManager = {
        let instance = CartManager()
        return instance
        
    }()
    
    private init() {
        
    }
}
