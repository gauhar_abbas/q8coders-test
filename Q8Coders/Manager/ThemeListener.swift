//
//  ThemeManager.swift
//  Elixir
//
//  Created by Praveen Sharama on 15/10/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import Foundation
import Themeable


extension UIColor {
    
    func colorFromHexString (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

// Define the theme and its properties to be used throughout your app
public struct AppTheme: Theme {
    
    public let identifier: String
    public let seperatorColor: UIColor
    public let backgroundColor: UIColor
    public let titleTextColor: UIColor
    public let subTitleTextColor: UIColor
    public let themeColor: UIColor
    public let lightBackgroundColor: UIColor
    public let statusBarStyle: UIStatusBarStyle
    public let barStyle: UIBarStyle
    
    static let light = AppTheme(
        identifier: "com.ios.Elixir.light-theme",
        seperatorColor: .lightGray,
        backgroundColor: .white,
        titleTextColor: .darkText,
        subTitleTextColor: .lightText,
        themeColor: .blue,
        lightBackgroundColor: .white,
        statusBarStyle: .default,
        barStyle: .default
    )
    
    static let dark = AppTheme(
        identifier: "com.ios.Elixir.dark-theme",
        seperatorColor: .black,
        backgroundColor: .white,
        titleTextColor: .darkText,
        subTitleTextColor: .lightText,
        themeColor: .blue,
        lightBackgroundColor: .gray,
        statusBarStyle: .lightContent,
        barStyle: .black
    )
    
    // Expose the available theme variants
    public static let variants: [AppTheme] = [ .light, .dark ]
    
    // Expose the shared theme manager
    public static let manager = ThemeManager<AppTheme>(default: .light)
    
}

public class ThemeListener : Themeable {
    
    public static var inst: ThemeListener = {
        let instance = ThemeListener()
        return instance
        
    }()
    
    private init() {
        AppTheme.manager.register(themeable: self)
    } 
    
    // function will be called whenever the theme changes
    public func apply(theme: AppTheme) {
        
        // You get your current (selected) theme and apply the main color to the tintColor property of your application’s window.
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = theme.backgroundColor
        
        UINavigationBar.appearance().barStyle = theme.barStyle
        UINavigationBar.appearance().barTintColor = theme.backgroundColor
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : theme.titleTextColor]
        
    }
}
