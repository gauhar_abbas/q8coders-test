//
//  SignUpController.swift
//  Elixir
//
//  Created by Afgan on 10/14/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import UIKit
import MBProgressHUD

class VerificationController: UIViewController{
  
  @IBOutlet var phoneNumber:UILabel!
  @IBOutlet var message:UILabel!
  @IBOutlet var editPhoneNumberButton:UIButton!
  @IBOutlet var otpTxtField:UITextField!
  @IBOutlet var verifyPhoneNumberButton:UIButton!
  @IBOutlet var resendOTPButton:UIButton!
  
  var mobileNumber :String?//passed phone from register screen
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    //setting UI
    self.title = "Verification"
    phoneNumber.text = mobileNumber ?? ""
    message.attributedText = NSAttributedString.init(string: "Please enter the verification code\nrecieved by sms.")
    editPhoneNumberButton.layer.cornerRadius = 12.0
    verifyPhoneNumberButton.layer.cornerRadius = 12.0
    resendOTPButton.layer.cornerRadius = 12.0
    editPhoneNumberButton.layer.borderColor = #colorLiteral(red: 0, green: 0.7490196078, blue: 0.7882352941, alpha: 1)
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationItem.setHidesBackButton(true, animated:true);
  }
  //push to back if user want to edit his phone
  @IBAction func editPhoneNumberButtonPressed(_ sender:UIButton){
    navigationController?.popViewController(animated: true)
  }
  @IBAction func verifyPhoneNumberButtonPressed(_ sender:UIButton){
    otpTxtField.resignFirstResponder()
    let phone = UserDefaults.standard.value(forKey: "phone") as? String ?? ""
    let otp = otpTxtField.text ?? ""
    if phone == "" || otp == ""{ //phone n number validations
      self.view.makeToast("Please enter OTP")
      return
    }
    MBProgressHUD.showAdded(to: self.view, animated: true)
    AccountManager.inst.verifyOTP(phone:phone, otp: otp, success: { (response) in
      Logger.log.debug("SUCCESS :: \(response)")
      
      MBProgressHUD.hide(for: self.view, animated: true)
      let delegate = UIApplication.shared.delegate as! AppDelegate
      delegate.appInitializer?.rootWireframe.showCartModuleAsRoot()
      if let tempDic = response.rawValue as? NSDictionary{
        let phone = tempDic["phone"] as? String ?? ""
        let id = tempDic["id"] as? Int ?? 0
        UserDefaults.standard.set(true, forKey: "isLoggedIn")
        UserDefaults.standard.set(phone, forKey: "phone")
        UserDefaults.standard.set(id, forKey: "owner_id")
      }
    }) { (error) in // in case of service fail or any type of error
      MBProgressHUD.hide(for: self.view, animated: true)
        self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid request")
      Logger.log.debug("ERROR :: \(String(describing: error))")
    }
  }
    
    // if user ddnt get  OTP then resend OTP to phone
  @IBAction func resendOTPButtonPressed(_ sender:UIButton){
    MBProgressHUD.showAdded(to: self.view, animated: true)
    let splitPhone = mobileNumber?.split(separator: " ")
    let countryCode = String(splitPhone?[0] ?? "")
    let phone = String(splitPhone?[1] ?? "")
    AccountManager.inst.registerPhone(country:countryCode, phone: phone, success: { (response) in
      Logger.log.debug("SUCCESS :: \(response)")
      
      MBProgressHUD.hide(for: self.view, animated: true)
      self.view.makeToast("OTP sent successfully")
    }) { (error) in
      MBProgressHUD.hide(for: self.view, animated: true)
      self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid request")
      Logger.log.debug("ERROR :: \(String(describing: error))")
    }  }
}
