//
//  ExLoginController.swift
//  Elixir
//
//  Created by Afgan on 10/7/18.
//  Copyright © 2018 Elixir. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toast_Swift

class RegisterController: UIViewController{
  
  @IBOutlet var countryCodeTxtView:UIView!
  @IBOutlet var phoneTxtView:UIView!
  
  @IBOutlet var countryCodeTxtField:UITextField!
  @IBOutlet var phoneTxtField:UITextField!
  
  @IBOutlet var registerButton:UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //turns placeholder color to white
    countryCodeTxtField.attributedPlaceholder = NSAttributedString(string: "Country Code",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
    phoneTxtField.attributedPlaceholder = NSAttributedString(string: "Enter Phone No",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightText])
    registerButton.layer.cornerRadius = 12.0
  }
  
  @IBAction func didRegisterButtonPressed(_ sender:UIButton){
    phoneTxtField.resignFirstResponder()
    countryCodeTxtField.resignFirstResponder()
    //check for country code and mobile number validations
    if let countryCode = countryCodeTxtField.text,let phone = phoneTxtField.text {
      if countryCode != "" && phone != ""{
        if phone.isPhone(){
          MBProgressHUD.showAdded(to: self.view, animated: true)
          AccountManager.inst.registerPhone(country:countryCode, phone: phone, success: { (response) in
            Logger.log.debug("SUCCESS :: \(response)")
            self.view.makeToast("registered successfully")
            MBProgressHUD.hide(for: self.view, animated: true)
            let verificationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerificationController") as! VerificationController
            verificationController.mobileNumber = countryCode + " " + phone; self.navigationController?.pushViewController(verificationController, animated: true)
            if let tempDic = response.rawValue as? NSDictionary{
              let phone = tempDic["phone"] as? String ?? ""
              let id = tempDic["id"] as? Int ?? 0
              UserDefaults.standard.set(phone, forKey: "phone")
              UserDefaults.standard.set(id, forKey: "owner_id")
            }
          }) { (error) in
            self.view.makeToast(error == nil ? "Internet Connection Error" : "Invalid Country Code")
            MBProgressHUD.hide(for: self.view, animated: true)
            Logger.log.debug("ERROR :: \(String(describing: error))")
          }
        } else {self.view.makeToast("Invalid phone")} // check for valid phone number
      }  else {self.view.makeToast("Invalid code/phone")}// check code or phone is empty
    } else {self.view.makeToast("Invalid code/phone")}// end of if //nil check for country code and phone
  } // end of the function
}
